John Kenneth Bautista

Motivation In Joining the Bootcamp
To begin with, I have a degree in BS Mathematics with specialization in Computer Science and after I graduated last 2019 I fall to an admistration role instead of any software development jobs. So right now, I decided to continue what I studied for. Also, I just wanted to have a work-from-home job as a software developer.

Work Experience
Since I graduated last 2019, I started working as administration staff in a telecom industry for 2 years from 2019 to 2021. In addition, I got an offer from the other company with a niche in construction and worked there for a year as an admistration assistant. Furthermore, I received again an offer for an Asset Management Specialist to a BPO company which they're supporting Australian company remotely. I worked there for almost 6 months and when they decided to let us work on-site I started to think to grab the opportunity offered by Zuitt for Coding Bootcamp to upskill my coding abilities and to have a system developer job after graduation, I'm currently 9 months now with then and I quit just to focus with the bootcamp.